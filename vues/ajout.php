
<html>
    <head>
       <meta charset="utf-8">
        <!-- importer le fichier de style -->
        <link rel="stylesheet" href="../CSS/style_ajout.css" media="screen" type="text/css" />
    </head>
    <body>
        <div id="container">
            <a href="../index.php"><input type="button" id="accueil" value="Accueil" ></a>
            <!-- zone de connexion -->
            
            <form action="../modeles/modele_ajout.php" method="POST">
                <center><h1>Ajout d'un Film</h1></center>
                <label><b>ID</b></label>
                <input type="text" placeholder="Entrer l'ID du film" name="id">

                <label><b>Nom du film</b></label>
                <input type="text" placeholder="Entrer le nom du film" name="nom" required>

                <label><b>Année</b></label>
                <input type="text" placeholder="Entrer l'année du film" name="annee" required>

                <label><b>Score</b></label>
                <input type="text" placeholder="Entrer le score du film" name="score" required>

                <label><b>NbVotants</b></label>
                <input type="text" placeholder="Entrer le nombre de votants" name="nbVotants" required>

                <input type="submit" id='submit' value='Ajouter' >
            </form>
        </div>
    </body>
</html>
